import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideolistpagePage } from './videolistpage.page';

describe('VideolistpagePage', () => {
  let component: VideolistpagePage;
  let fixture: ComponentFixture<VideolistpagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideolistpagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideolistpagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
